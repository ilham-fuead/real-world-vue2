import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost/evapi',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
  },
  withCredentials: false,
})

export default {
  getEvents() {
    return apiClient.get('/events.php')
  },
  getEvent(id) {
    return apiClient.get('/event.php?id=' + id)
  },
}
